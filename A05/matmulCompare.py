import numpy

import sys
sys.path.append('/repo459/A04')
from pyMatmul import matmul

import argparse

import random

import time

import matplotlib.pyplot as plt


#b)
parser = argparse.ArgumentParser()
parser.add_argument("input", type = int, choices = [0,1], help = "input 0 or 1 to retrieve boolean equivalent", metavar = "plotBool")
plotB = parser.parse_args()
if plotB.input == 1:
    plotBool = True
else:
    plotBool = False

#c)
#I used this function to create a matrix in timeMatmul.py
def createMatrix(dimension, inputValue):
    matrix = []
    for i in range(dimension):
        row = []
        for j in range(dimension):
            row.append(inputValue[dimension * i + j])
        matrix.append(row)
    return matrix

nD = []
timeOFmatmul = []
timeOFnumpy = []

## n = 2**5
A_list = []
n = 2**5
nD.append(n)
for i in range(0,n**2):
    givenRange = random.uniform(-1,1)
    A_list.append(givenRange)
A_list = createMatrix(n,A_list)
A = numpy.array(A_list)

B_list = []
for i in range(0,n**2):
    givenRange = random.uniform(-1,1)
    B_list.append(givenRange)
B_list = createMatrix(n,B_list)
B = numpy.array(B_list)

#d) i.
start = time.perf_counter()
matmul(A_list,B_list)
end = time.perf_counter()
timeTaken_s = end - start
timeTaken_ms = timeTaken_s*1000
timeOFmatmul.append(timeTaken_ms)
    # print("d)i. n=2\N{SUPERSCRIPT FIVE}:",timeTaken_ms)

#d) ii.
start = time.perf_counter()
C = A @ B
end = time.perf_counter()
timeTaken_ms = (end-start) * 1000
timeOFnumpy.append(timeTaken_ms)
    # print("d)ii. n=2\N{SUPERSCRIPT FIVE}:",timeTaken_ms)

## n = 2**6
A_list = []
n = 2**6
nD.append(n)
for i in range(0,n**2):
    givenRange = random.uniform(-1,1)
    A_list.append(givenRange)
A_list = createMatrix(n, A_list)
A = numpy.array(A_list)

B_list = []
for i in range(0,n**2):
    givenRange = random.uniform(-1,1)
    B_list.append(givenRange)
B_list = createMatrix(n,B_list)
B = numpy.array(B_list)

#d) i.
start = time.perf_counter()
matmul(A_list,B_list)
end = time.perf_counter()
timeTaken_s = end - start                        
timeTaken_ms = timeTaken_s*1000
timeOFmatmul.append(timeTaken_ms)
    # print("d)i. n=2\N{SUPERSCRIPT SIX}:",timeTaken_ms)

#d) ii.
start = time.perf_counter()
C = A @ B
end = time.perf_counter()
timeTaken_ms = (end-start) * 1000
timeOFnumpy.append(timeTaken_ms)
    # print("d)ii. n=2\N{SUPERSCRIPT SIX}:",timeTaken_ms)

## n = 2**7
A_list = []
n = 2**7
nD.append(n)
for i in range(0,n**2):
    givenRange = random.uniform(-1,1)
    A_list.append(givenRange)
A_list = createMatrix(n, A_list)
A = numpy.array(A_list)

B_list = []
for i in range(0,n**2):
    givenRange = random.uniform(-1,1)
    B_list.append(givenRange)
B_list = createMatrix(n,B_list)
B = numpy.array(B_list)

#d) i.
start = time.perf_counter()
matmul(A_list,B_list)
end = time.perf_counter()
timeTaken_s = end - start
timeTaken_ms = timeTaken_s*1000
timeOFmatmul.append(timeTaken_ms)
    # print("d)i. n=2\N{SUPERSCRIPT SEVEN}:",timeTaken_ms)

#d) ii.
start = time.perf_counter()
C = A @ B
end = time.perf_counter()
timeTaken_ms = (end-start) * 1000
timeOFnumpy.append(timeTaken_ms)
    # print("d)ii. n=2\N{SUPERSCRIPT SEVEN}:",timeTaken_ms)

## n = 2**8
A_list = []
n = 2**8
nD.append(n)
for i in range(0,n**2):
    givenRange = random.uniform(-1,1)
    A_list.append(givenRange)
A_list = createMatrix(n, A_list)
A = numpy.array(A_list)

B_list = []
for i in range(0,n**2):
    givenRange = random.uniform(-1,1)
    B_list.append(givenRange)
B_list = createMatrix(n,B_list)
B = numpy.array(B_list)

#d) i.
start = time.perf_counter()
C_list = matmul(A_list,B_list)
end = time.perf_counter()
timeTaken_s = end - start
timeTaken_ms1 = timeTaken_s*1000
timeOFmatmul.append(timeTaken_ms1)
    # print("d)i. n=2\N{SUPERSCRIPT EIGHT}:",timeTaken_ms)

#d) ii.
start = time.perf_counter()
C = A @ B
end = time.perf_counter()
timeTaken_ms2 = (end-start) * 1000
timeOFnumpy.append(timeTaken_ms2)
    # print("d)ii. n=2\N{SUPERSCRIPT EIGHT}:",timeTaken_ms)

#e)
print(round(C_list[0][0],4))
print(round(timeTaken_ms1,4))
print(round(C[0][0],4))
print(round(timeTaken_ms2,4))

#print(nD)
#print(timeOFmatmul)
#print(timeOFnumpy)
#print(plotBool)

#f)
if plotBool == True:
    plt.loglog(nD,timeOFmatmul, label = "Time of Matmul Function [ms]")
    plt.loglog(nD,timeOFnumpy, label = "Time of Numpy [ms]")
    plt.xscale("log",base=2)
    plt.yscale("log",base=10)
    plt.xlabel("log\u2082(n)")
    plt.ylabel("log\u2081\u2080(t)")
    plt.legend()
    plt.title("Time of Matmul & Numpy vs. Matrix Size")
    plt.savefig('A05_task1_plt.png')
else:
    None
