
## can't use init, str, call, add, lt, doc, cmp, len, call
## google 'dunder methods'

class Person:
    def __init__(self,firstName = 'Jane', lastName = 'Doe',sex = 'female', eyeColor= 'brown',job = 'unemployed',annualIncome = 60000):
        self.firstName = firstName
        self.lastName = lastName
        self.sex = sex
        self.eyeColor = eyeColor
        self.job = job
        self.annualIncome = annualIncome

    def __str__(self):
        return f"Name: {self.firstName + ' ' + self.lastName}\nSex: {self.sex}\nEye Color: {self.eyeColor}\nJob: {self.job}\nAnnual Income: {self.annualIncome}"

    def __gt__(self,other):
        return self.annualIncome > other.annualIncome

    def __getitem__(self,arg):
        if isinstance(arg,str):
            keys = [arg]
        else:
            keys = list(arg)
        return [self.__dict__[key] for key in keys]
    # https://stackoverflow.com/questions/51735399/how-to-implement-the-getitem-dunder-method-in-python-to-get-attribute-values

    def __init_subclass__(cls,/,default, category, **kwargs):
        super().__init_subclass__(**kwargs)
        cls.default = default    
        cls.default = category

class Student(Person ,default = 'Junior',category = 'Upper Classmen'):                                                                                      pass
                                                                                    
class Age(Person, default = 26, category = "twenties"):
    def __init__(self,age = '26', category = 'twenties'):
        self.age = age
        self.category = category
    def __str__(self):
        return f"Age: {self.age}\nCategory: {self.category}"

if __name__ == "__main__":
    jD = Person()
    jS = Person('John','Smith','male','blue','student',7500)
    aC = Person('Agatha','Christie', 'female','green','writer',100000)
    print("jS > jD", jS>jD)
    print("aC > jD", aC>jD)
    firstNames = jS['firstName']+ jD['firstName'] + aC['firstName']
    print(firstNames)

    sb1 = Student()
    print(sb1)
    sb2 = Age()
    print(sb2)

