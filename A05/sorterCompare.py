##TASK 2
#(a)
import numpy
import sys
#sys.path.append('/repo459/A04')
from pySort import sorter
import argparse
import random
import copy
import time
import matplotlib.pyplot as plt

#(b)
parser = argparse.ArgumentParser()
parser.add_argument('input', type = int, choices = [0,1], help = "input 0 or 1 to retrieve boolean equivalent", metavar = "plotBool")
plotB = parser.parse_args()
if plotB.input == 1:
    plotBool = True
else:
    plotBool = False


nD = []
timeOFsorter = []
timeOFpython = []
timeOFnumpy = []

#(c) n = 2**10
A_list = []
n = 2**10
nD.append(n)
for i in range(0,n**2):
    givenRange = random.randint(-10,10)
    A_list.append(givenRange)
B_list = copy.deepcopy(A_list)
A = numpy.array(A_list)

#(d) i.
start = time.perf_counter()
sorter(A_list)
end = time.perf_counter()
timeTaken = (end-start)* 1000 #in milliseconds
timeOFsorter.append(timeTaken)

#(d) ii.
start = time.perf_counter()
B_list.sort()
end = time.perf_counter()
timeTaken = (end-start)*1000 #in milliseconds
timeOFpython.append(timeTaken)

#(d) iii.
start = time.perf_counter()
A = numpy.sort(A)
end = time.perf_counter()
timeTaken = (end-start)*1000 #in milliseconds
timeOFnumpy.append(timeTaken)

#(c) n = 2**12
A_list = []
n = 2**12
nD.append(n)
for i in range(0,n**2):
    givenRange = random.randint(-10,10)
    A_list.append(givenRange)
B_list = copy.deepcopy(A_list)
A = numpy.array(A_list)

#(d) i.
start = time.perf_counter()
sorter(A_list)
end = time.perf_counter()
timeTaken = (end-start)* 1000 #in milliseconds
timeOFsorter.append(timeTaken)

#(d) ii.
start = time.perf_counter()
B_list.sort()
end = time.perf_counter()
timeTaken = (end-start)*1000 #in milliseconds
timeOFpython.append(timeTaken)

#(d) iii.
start = time.perf_counter()
A = numpy.sort(A)
end = time.perf_counter()
timeTaken = (end-start)*1000 #in milliseconds
timeOFnumpy.append(timeTaken)

#(c) n = 2**14
A_list = []
n = 2**14
nD.append(n)
for i in range(0,n**2):
    givenRange = random.randint(-10,10)
    A_list.append(givenRange)
B_list = copy.deepcopy(A_list)
A = numpy.array(A_list)

#(d) i.
start = time.perf_counter()
sorter(A_list)
end = time.perf_counter()
timeTaken1 = (end-start)* 1000 #in milliseconds
timeOFsorter.append(timeTaken1)

#(d) ii.
start = time.perf_counter()
B_list.sort()
end = time.perf_counter()
timeTaken2 = (end-start)*1000 #in milliseconds
timeOFpython.append(timeTaken2)

#(d) iii.
start = time.perf_counter()
A = numpy.sort(A)
end = time.perf_counter()
timeTaken3 = (end-start)*1000 #in milliseconds
timeOFnumpy.append(timeTaken3)

#(e)
print(A_list[0])
print(round(timeTaken1,4))
print(B_list[0])
print(round(timeTaken2,4))
print(A[0])
print(round(timeTaken3,4))


#(f)
if plotBool == True:
    plt.loglog(nD,timeOFsorter, label = "Time of Sorter Function [ms]")
    plt.loglog(nD,timeOFpython, label = "Time of Python [ms]")
    plt.loglog(nD,timeOFnumpy, label = "Time of Numpy [ms]")
    plt.xscale("log",base=2)
    plt.yscale("log",base=10)
    plt.xlabel("log\u2082(n)")
    plt.ylabel("log\u2081\u2080(t)")
    plt.legend()
    plt.title("Time of Sorter, Pyhton, & Numpy vs. List Size")
    plt.savefig('A05_task2_plt.png')
else:
    None

