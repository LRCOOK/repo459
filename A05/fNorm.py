## Task3 ##
import numpy as np
import argparse

#(a)
parser = argparse.ArgumentParser()
parser.add_argument("name", type = str)
args = parser.parse_args()
filename = args.name + '.csv'
arr = np.loadtxt(filename,delimiter = ",", dtype = float)

#(b)
FrobeniusNorm = round(np.linalg.norm(arr, 'fro'),3)
print(FrobeniusNorm)
