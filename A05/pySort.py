#I had to redo my sorter function from the previous homework. I referenced the code here: https://stackoverflow.com/questions/11964450/order-a-list-of-numbers-without-built-in-sort-min-max-function

def sorter(unsortedList):
    sortedList = []
    minimum = unsortedList[0]
    for i in unsortedList:
        if i < minimum:
            minimum = i
    sortedList.append(minimum)
    unsortedList.remove(minimum)

