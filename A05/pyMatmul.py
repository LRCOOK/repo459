def matmul(A,B):
    answer = []
    for i in range(len(A)):   #rows of A
        row = []
        for j in range(len(B[0])): # col of B
            value = 0
            for k in range(len(B)):    #row of B 
                value += A[i][k] * B[k][j]
            row.append(value)
        answer.append(row)
    return answer

if __name__ == "__main__":
    A = [[1,2,3],[4,5,6],[7,8,9]]
    B = [[10,11,12],[13,14,15],[16,17,18]]
    C = matmul(A,B)
    print(C)

    # references I looked at: https://pyquestions.com/python-3-multiply-a-vector-by-a-matrix-without-numpy
    # https://www.programiz.com/python-programming/examples/multiply-matrix

