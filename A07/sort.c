#include "sort.h"

/*https://www.geeksforgeeks.org/insertion-sort/*/

void sort(int* A, size_t n_elements) {
    int i, key, j;
    for (i = 1; i < n_elements; i++){
        key = A[i];
        j = i - 1;

        while (j >= 0 && A[j] > key){
            A[j + 1] = A[j];
            j = j - 1;
        }
        A[j + 1] = key;
    }
}

