#include "mvmul.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

/*https://www.geeksforgeeks.org/generating-random-number-range-c/*/
/*https://stackoverflow.com/questions/33058848/generate-a-random-double-between-1-and-1*/
double randfrom(double min, double max) 
{
    double range = max - min; 
    double nom = (double) rand() / RAND_MAX;
    double val = range * nom;
    val += min;
    return val;
}

int main(int argc, char *argv[]) {
    int n;
    n = atoi(argv[1]);
    double* A;
    A = (double* ) malloc ( (n*n) * sizeof(double) );
    double* b;
    b = (double* ) malloc ( n * sizeof(double) );
    double* c;
    c = (double* ) malloc ( n * sizeof(double) );

    srand (time (NULL) );
    int i;
    for (i = 0; i < (n*n); i++){
        A[i] = randfrom(-1.,1.);
    }
    /*printf("Values in A: ");
    for (i = 0; i < (n*n); i++){
        printf("%f ", A[i]);
    }  
    printf("\n");*/
    
    /*printf("Values in b: ");*/
    for (i = 0; i < n; i++){
        b[i] = 1.;
        /*printf("%f ", b[i]);*/
    }
    /*printf("\n");*/
    /*https://www.geeksforgeeks.org/how-to-measure-time-taken-by-a-program-in-c/*/

    clock_t start, end;
    double time_taken;
    start = clock();
    mvmul(A, b, c, n);
    end = clock();
    time_taken = ( (double) (end - start)) / CLOCKS_PER_SEC;

    /*printf("Values in c: ");
    for (i = 0; i < n; i++) {
        printf("%f ", c[i]);
    }
    printf("\n");*/

    /*printf("Last value in c: ");*/
    printf("%f ", c[n-1]);
    printf("\n");

    printf("%f ", time_taken);
    printf("\n");
    

    free(A);
    free(b);
    free(c);
}