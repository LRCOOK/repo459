
class Student:

    def __init__(self, lastName = "Popescu", gpa = 3.8):

        if type(lastName) != str:
            print("Input of lastName must be a string.")
            lastName = "Popescu"
        self.lastName = lastName

        if type(gpa) != float or gpa<0 or gpa>4:
            print("Input of gpa must be an integer between 0 and 4.")
            gpa = 3.8
        self.gpa=gpa
    
    def __str__(self):
        dummy = self.lastName
        dummy +=" | gpa: "
        dummy +='%s' % self.gpa
        return dummy
    
    def student_info(self):
        print("Student Last Name: ", self.lastName)
        print("Student GPA:", self.gpa)

    def compareGPA(self,other):
        if self.gpa > other.gpa:
            print(self.lastName)
        else:
            print(other.lastName)

if __name__== "__main__":
    s1 = Student()
    s2 = Student("Evans",2.8)
    s3 = Student("Cook", 3.5)
    print(s3)
    s1.compareGPA(s2)
    s1.compareGPA(s3)
    s2.compareGPA(s3)
