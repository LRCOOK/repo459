from pyMatmul import matmul

import timeit
import functools

def createMatrix(n, inputValue):
    matrix = []
    for i in range(n):
        row = []
        for j in range(n):
            row.append(inputValue[n * i + j])
        matrix.append(row)
    return matrix

inputValue2to7 = []
n2to7 = 2**7
inputValue2to7.extend(range(0,n2to7**2))
Aval = createMatrix(n2to7,inputValue2to7)
Bval = createMatrix(n2to7,inputValue2to7)

inputValue2to8 = []
n2to8 = 2**8
inputValue2to8.extend(range(0,n2to8**2))
Cval = createMatrix(n2to8,inputValue2to8)
Dval = createMatrix(n2to8,inputValue2to8)

inputValue2to9= []
n2to9 = 2**9
inputValue2to9.extend(range(0,n2to9**2))
Eval = createMatrix(n2to9,inputValue2to9)
Fval = createMatrix(n2to9,inputValue2to9)

time2to7 = timeit.Timer(functools.partial(matmul,Aval,Bval))
time2to8 = timeit.Timer(functools.partial(matmul,Cval,Dval))
time2to9 = timeit.Timer(functools.partial(matmul,Eval,Fval))

print("n=2\N{SUPERSCRIPT SEVEN}:",time2to7.timeit(1))
print("n=2\N{SUPERSCRIPT EIGHT}:",time2to8.timeit(1))
print("n=2\N{SUPERSCRIPT NINE}:",time2to9.timeit(1))

# referened https://stackoverflow.com/questions/40120892/creating-a-matrix-in-python-without-numpy
