from pySort import sorter

import timeit
import functools
import random

Alist = []
for i in range(0,10**3):
    n = random.randint(1,100)
    Alist.append(n)

time10to3 = timeit.Timer(functools.partial(sorter,Alist))
print("First element:",Alist[0])
print("n=10\N{SUPERSCRIPT THREE}:", time10to3.timeit(1))
print("Sorted first element:",Alist[0])

Blist = []
for i in range(0,10**4):
    n = random.randint(1,100)
    Blist.append(n)

time10to4 = timeit.Timer(functools.partial(sorter,Blist))
print("First element:",Blist[0])
print("n=10\N{SUPERSCRIPT FOUR}:", time10to4.timeit(1))
print("Sorted first element:",Blist[0])

Clist = []
for i in range(0,10**5):
    n = random.randint(1,100)
    Clist.append(n)
time10to5 = timeit.Timer(functools.partial(sorter,Clist))
print("First element:",Clist[0])
print("n=10\N{SUPERSCRIPT FIVE}:", time10to5.timeit(1))
print("Sorted first element:",Clist[0])


Dlist = []
for i in range(0,10**6):
    n = random.randint(1,100)
    Dlist.append(n)
time10to6 = timeit.Timer(functools.partial(sorter,Dlist))
print("First element:",Dlist[0])
print("n=10\N{SUPERSCRIPT SIX}:", timeto6.timeit(1))
print("Sorted first element:",Dlist[0])
