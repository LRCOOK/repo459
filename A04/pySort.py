
def sorter(A):
    size = len(A)
    swapped = False

    for i in range(size):
        for j in range(0,size-i-1):
            if A[j] > A[j+1]:
                swapped = True
                A[j], A[j+1] = A[j+1], A[j]

        if not swapped:
            return None

# referenced: https://www.geeksforgeeks.org/python-program-for-bubble-sort/

if __name__== "__main__":
    A = [23,21,27,34,29,31]
    print(A)
    sorter(A)
    print(A)
