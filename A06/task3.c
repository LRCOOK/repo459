#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>

/*struct array {
    int height;
    int width;
    int *arrayN;
};*/

int cmpfunc (const void * a, const void * b)
{
    return ( *(int*)b - *(int*)a );
}

int main (int argc, char *argv[])
{   /*a*/
    if (argc == 1) {
        /*printf("No input has been provided.\n"); */
        return 0;
    }
    
    if (argc == 2) {
        int N;
        N = atoi(argv[1]);
        if (N <= 0) {
            return 0;
        }
        if (N > 0) {
        /*    printf("argc: %d", argc);
            printf("\nWelcome, %s\n", argv[1]);
            printf("String value = %s, N = %d\n", argv[1], N);
        */
            /*struct array skinnyMatrix;
            skinnyMatrix.height = 1;
            skinnyMatrix.width = N;
            skinnyMatrix.arrayN = (int*) malloc((N+1) * sizeof(int)); 
            int i, j;
            for (int i = 0; i < skinnyMatrix.height;  i++) 
                for (j = 0; j < skinnyMatrix.width; j++)
                    skinnyMatrix.arrayN[i * skinnyMatrix.height + j] = 1. /(i + j + 1.);
            
            free(skinnyMatrix.arrayN);
            */
            /*b*/
            int * arrayN;
            arrayN = (int*) malloc((N+1) * sizeof(int));
            int i, array[N+1];
            for ( i = 0; i < (N+1); i++) {
                /*c*/
                array[i] = i ;
                /*d*/
                printf("%d ", array[i]);
            }
             printf ("\n");
        /*e*/ 
            qsort(array, sizeof(array)/sizeof(array[0]), sizeof(array[0]), cmpfunc);
            for (int i=0; i < sizeof(array)/sizeof(array[0]); i++) {
                /*f*/
                printf("%d ", array[i]);
                }
        /*https://stackoverflow.com/questions/55672790/why-qsort-int-array-in-descending-order-is-incorrect*/
            printf ("\n");            
            /*g*/
            free(arrayN);
        }
        }
        

return 0;
}

/*
int main(void) {
    int num;
    if (scanf("%d", &num) != 1 || num <= 0) {
        printf("Error: please enter a positive integer");
    }
return 0;
}
*/
