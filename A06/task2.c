/*a*/
#include "structs.h"
#include <stdio.h>
#include <stdlib.h>

typedef struct {int i; char c; double d; } A;
typedef struct {int i; char d;  double c; } B;

int main() 
{
	/*b*/
	printf("\nSize of struct A: %lu\n", sizeof(struct A));
	printf("Size of struct B: %lu\n", sizeof(struct B));

	/*c*/
	struct A * allocatedStruct;
	allocatedStruct = (struct A*)malloc(sizeof(struct A));
	
	/*d*/
	struct A newFields = {5,'L', 12345679.101112};

	/*e*/
	printf("\nContents of struct A are: \n");
	printf("int: %d\n", newFields.i);
	printf("char: %c\n", newFields.c);
	printf("double: %f\n", newFields.d);

	/*f*/
	free(allocatedStruct);
	return 0;	
};

