#include <stdio.h>
#include <limits.h>

int main() {
    int a;

    a = INT_MAX;
    // printf("a = %d\n", a);

    a += 1;
    // printf("a = %d\n", a);

    a = INT_MIN;
    // printf("a = %d\n", a);

    a -= 1;
    // printf("a = %d\n", a);

    return 0;
}

/*https://www.tutorialspoint.com/c_standard_library/limits_h.htm*/