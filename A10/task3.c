#include <stdio.h>
#include <math.h>

float g(float x){
    return sin(x) / x;
}

int main(){
    float a = 0.0;
    float b = 1.0;

    int maxIter = 1000;
    int iter = 0;
    float precision = 0.0000001;

    while (iter < maxIter && (b - a) > precision) {
        float c = (a+b) / 2.0;
        if (g(c) > 1.0){
            b = c;
        }
        else {
            a = c;
        }
        iter++;
    }
    printf("%.16f\n",a);

    return 0;
}

/*
double g(double x){
    return sin(x) / x;
}

int main(){
    double a = 0.0;
    double b = 1.0;

    int maxIter = 1000;
    int iter = 0;
    double precision = 0.000000000000001;

    while (iter < maxIter && (b - a) > precision) {
        double c = (a+b) / 2.0;
        if (g(c) > 1.0){
            b = c;
        }
        else {
            a = c;
        }
        iter++;
    }
    printf("%.16f\n",a);

    return 0;
}
*/

/*https://stackoverflow.com/questions/9765744/precision-in-c-floats*/