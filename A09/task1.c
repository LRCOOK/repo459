#include "matmul.h"
#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#define MAX_VAL 1.0
#define MIN_VAL -1.0

int main(int argc, char * argv[]) {
    if (argc != 2){
        return 1;
    }
    
    size_t n = atoi(argv[1]);

    double * A = malloc( (n*n) * sizeof(double));
    double * B = malloc( (n*n) * sizeof(double));
    double * C = malloc( (n*n) * sizeof(double));

    srand(time(NULL));

    for (int i = 0; i < (n*n); i++) {
        A[i] = (double) rand() / RAND_MAX * (MAX_VAL - MIN_VAL) + MIN_VAL;
        B[i] = (double) rand() / RAND_MAX * (MAX_VAL - MIN_VAL) + MIN_VAL;
    }

    clock_t start = clock();
    mmul1(A,B,C,n);
    clock_t end = clock();
    double time_taken = (double) (end - start) / CLOCKS_PER_SEC;

    printf("%f ", time_taken * 1000);
    printf("\n");
    printf("%f ", C[(n*n)-1]);
    printf("\n");

    free(A);
    free(B);
    free(C);

    return 0;
}
/*Used: https://www.geeksforgeeks.org/cc-preprocessors/
        https://en.cppreference.com/w/c/numeric/random/RAND_MAX     
        https://en.cppreference.com/w/c/numeric/random/rand

*/