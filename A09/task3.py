#task3

import numpy
import time

#i
A = numpy.random.uniform(-1, 1, size = (3400, 1400))
B = numpy.random.uniform(-1, 1, size = (3400, 1400))

#ii
print(f'{A[-1][-1]}, {B[-1][-1]}')

#iii
start = time.time()
for i in range(len(A)):
    for j in range(len(A[0])):
        value = A[i][j]
        A[i][j] = B[i][j]
        B[i][j] = value
end = time.time()

timeTaken = (end - start) * 1000

#iv
print(f'{A[-1][-1]}, {B[-1][-1]}')
print(timeTaken)

#v
start = time.time()
for j in range(len(A[0])):
    for i in range(len(A)):
        value = A[i][j]
        A[i][j] = B[i][j]
        B[i][j] = value
end = time.time()

timeTaken = (end - start) * 1000

#vi
print(f'{A[-1][-1]}, {B[-1][-1]}')
print(timeTaken)