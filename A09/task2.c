#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#define n_elems 512*1024/sizeof(double)              /*512 KB*/
#define MAX_VAL 1.0
#define MIN_VAL -1.0

/*i*/
void test1(double * data, int elems, int stride){
    double result = 0.0;
    volatile double sink;

    for (int i = 0; i < elems; i += stride) {
        result += data[i];
    }

    sink = result;
    //printf("%f\n", result);
}
/*ii*/
void test2(double * data, int elems, int stride){
    volatile double result = 0.0;
    volatile double sink;

    for (int i = 0; i < elems; i += stride) {
        result += data[i];
    }

    sink = result;
    //printf("%f\n", result);
}

int main(){
    double * data = (double *) malloc(n_elems * sizeof(double));

    srand(time(NULL));
    /*iii*/
    for (int i = 0; i < n_elems; i++){
        data[i] = (double) rand() / RAND_MAX * (MAX_VAL - MIN_VAL) + MIN_VAL;
    }
    /*iv*/
    test1(data, n_elems, 1);    // Warm up cache

    /*test1*/
    double times1[7];
    int strideVal[7] = {1,2,4,8,11,15,17};

    for (int i = 0; i < 7; i++){
        int stride = strideVal[i];

        clock_t start = clock();
        for (int j = 0; j < 100; j++){
            test1(data, n_elems, stride);
        }
        clock_t end = clock();

        double timeTaken = (double) (end-start)/CLOCKS_PER_SEC;
        double avgTime =  timeTaken * (1000.0 / 100.0);
        times1[i] = avgTime;
        printf("%f ", times1[i]);
    }
    //printf("%f ", times1[7]);  
    printf("\n");

    /*test2*/
    double times2[7];
    test2(data, n_elems, 1);    // Warm up cache

    for (int i = 0; i < 7; i++){
        int stride = strideVal[i];

        clock_t start2 = clock();
        for (int j = 0; j < 100; j++){
            test2(data, n_elems, stride);
        }
        clock_t end2 = clock();

        double timeTaken2 = ((double) (end2-start2)) / CLOCKS_PER_SEC;
        double avgTime2 =  timeTaken2 * (1000.0 / 100.0);
        times2[i] = avgTime2;   
        printf("%f ", times2[i]);   
    }    
    printf("\n");
    // printf("%f ", times2[7]);   
    // printf("\n");
    free(data);
    return 0;
}