#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "sumArray.h"

/*https://www.geeksforgeeks.org/generating-random-number-range-c/*/
/*https://stackoverflow.com/questions/33058848/generate-a-random-double-between-1-and-1*/
double randfrom(double min, double max) 
{
    double range = max - min; 
    double nom = (double) rand() / RAND_MAX;
    double val = range * nom;
    val += min;
    return val;
}


int main(int argc, char *argv[]){
    int n;
    n = atoi(argv[1]);
    double* A;
    A = (double* ) malloc ( (n*n) * sizeof(double) );

    int i;
    for (i = 0; i < (n*n); i++){
    A[i] = randfrom(-1.,1.);
    }
    /*
    printf("Values in A: ");
    for (i = 0; i < (n*n); i++){
        printf("%f ", A[i]);
    }  
    printf("\n"); */

    srand(time (NULL));
    
    clock_t start, end;
    double time_taken1;
    start = clock();
    sumArray1(A,n);
    end = clock();
    time_taken1 = ( (double) (end - start)) / CLOCKS_PER_SEC;
    printf("t1 %f ", time_taken1*1000);
    printf("\n");
   
    double time_taken2;
    start = clock();
    sumArray2(A,n);
    end = clock();
    time_taken2 = ( (double) (end - start)) / CLOCKS_PER_SEC;
    printf("t2 %f ", time_taken2*1000);
    printf("\n");

    free(A);
}