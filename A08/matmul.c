#include <stdlib.h>
#include <stdio.h>
#include "matmul.h"

void mmul1(const double* A, const double* B, double *C, const size_t n){
    size_t i, j, k;
    for (i = 0; i < n; i++){
        for (j = 0; j < n; j++) {
            for (k=0; k < n; k++){
                C[i] += A[i * n + j] * B[k * j];
                /*I don't think this is correct, should prob be something like C[i][j] +=A[i][k] * B[k][j]*/
                /*I was having issues indexing because I don't think I was allocating memory and creating the matrix correctly.*/
            }
        }
    }
}

void mmul2(const double* A, const double* B, double *C, const size_t n){
    size_t i, j, k;
    for (k = 0; k < n; k++){
        for (j = 0; j < n; j++) {
            for (i=0; i < n; i++){
                C[i] += A[i * n + j] * B[k * j];
            }
        }
    }
}

void mmul3(const double* A, const double* B, double *C, const size_t n){
    size_t i, j, k;
    for (i = 0; i < n; i++){
        for (j = 0; j < n; j++) {
            for (k=0; k < n; k++){
                C[i] += A[i * n + j] * B[j * k];
                /*C[i][j] +=A[i][k] * B[j][k]*/
            }
        }
    }
}

void mmul4(const double* A, const double* B, double *C, const size_t n){
    size_t i, j, k;
    for (i = 0; i < n; i++){
        for (j = 0; j < n; j++) {
            for (k=0; k < n; k++){
                C[i] += A[j * n + i] * B[j * k];
                /*C[i][j] +=A[k][i] * B[j][k]*/
            }
        }
    }
}