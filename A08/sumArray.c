#include <stdlib.h>
#include <stdio.h>
#include "sumArray.h"

double sumArray1(const double* A, const size_t n) {
    int i, j;
    double sum = 0;
    for (i = 0; i < n; i++){
        for (j = 0; j < n; j++) {
            sum = sum + A[i * n + j];
        }
    }

    return sum;
}

double sumArray2(const double* A, const size_t n) {
    int i, j;
    double sum = 0;
    for (j = 0; j < n; j++){
        for (i = 0; i < n; i++) {
            sum += A[i * n + j];
        }
    }

    return sum;
}