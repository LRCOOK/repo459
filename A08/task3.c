#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "matmul.h"

/*https://www.geeksforgeeks.org/generating-random-number-range-c/*/
/*https://stackoverflow.com/questions/33058848/generate-a-random-double-between-1-and-1*/
double randfrom(double min, double max) 
{
    double range = max - min; 
    double nom = (double) rand() / RAND_MAX;
    double val = range * nom;
    val += min;
    return val;
}

int main(){
    double * A, B, C;
    int n = 1024;
    /*I need to allocate memory based on rows and collumns, should probably be double***/
    A = (double* ) malloc ( (n*n) * sizeof(double) );
    B = (double* ) malloc ( (n*n) * sizeof(double) );
    C = (double* ) malloc ( (n*n) * sizeof(double) );

    int i;
    for (i = 0; i < (n*n); i++){
        A[i] = randfrom(-1.,1.);
    }
    for (i = 0; i < (n*n); i++){
        B[i] = randfrom(-1.,1.);
    }
    for (i = 0; i < (n*n); i++){
       C[i] = 0; 
    }

    srand(time (NULL));

    clock_t start1, end1;
    double time_taken1;
    start1 = clock();
    mmul1(A,B,C,n);
    end1 = clock();
    time_taken1 = ( (double) (end1 - start1)) / CLOCKS_PER_SEC;
    printf("t1 %f ", time_taken1*1000);
    printf("\n");

    clock_t start2, end2;
    double time_taken2;
    start2 = clock();
    mmul2(A,B,C,n);
    end2 = clock();
    time_taken2 = ( (double) (end2 - start2)) / CLOCKS_PER_SEC;
    printf("t2 %f ", time_taken2*1000);
    printf("\n");

    clock_t start3, end3;
    double time_taken3;
    start3 = clock();
    mmul3(A,B,C,n);
    end3 = clock();
    time_taken3 = ( (double) (end3 - start3)) / CLOCKS_PER_SEC;
    printf("t3 %f ", time_taken3*1000);
    printf("\n");

    clock_t start4, end4;
    double time_taken4;
    start4 = clock();
    mmul4(A,B,C,n);
    end4 = clock();
    time_taken4 = ( (double) (end4 - start4)) / CLOCKS_PER_SEC;
    printf("t4 %f ", time_taken1*1000);
    printf("\n");

    free(A);
    free(B);
    free(C);
}
