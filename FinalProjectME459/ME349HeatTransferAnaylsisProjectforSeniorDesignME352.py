#ME349HeatTransferAnaylsisProjectforSeniorDesignME352

#ProjectCode

import csv
import datetime
import pandas as pd
import requests
import numpy as np
from scipy import interpolate
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import matplotlib.ticker as ticker
from CoolProp.CoolProp import PropsSI
from scipy.constants import convert_temperature
from thermo import Chemical
import math

#API
url = f'https://archive-api.open-meteo.com/v1/archive?latitude=38.6735&longitude=-77.1714&start_date=2012-01-01&end_date=2022-01-01&hourly=temperature_2m,relativehumidity_2m,dewpoint_2m,apparent_temperature,pressure_msl,surface_pressure,precipitation,cloudcover,shortwave_radiation,direct_radiation,diffuse_radiation,windspeed_10m,windspeed_100m,winddirection_10m,winddirection_100m,windgusts_10m&daily=temperature_2m_max,temperature_2m_min,temperature_2m_mean,apparent_temperature_max,apparent_temperature_min,apparent_temperature_mean,sunrise,sunset,shortwave_radiation_sum,precipitation_sum,rain_sum,snowfall_sum,precipitation_hours,windspeed_10m_max,windgusts_10m_max,winddirection_10m_dominant&timezone=America%2FNew_York&temperature_unit=fahrenheit&windspeed_unit=ms'
response = requests.get(url)
#print(response)
#data = response.json()
#print(data)
#df = pd.DataFrame(data)

#pandas attempt
#df = pd.read_csv('archive.csv', header = 2)
print(df['temperature_2m (°F)'])


##import csv attempt
with open('archive.csv') as csvfile:
    csvreader = csv.reader(csvfile)
    specifiedFields = next(csvreader)
    specifiedData = next(csvreader)
    empty = next(csvreader)
    fields = next(csvreader)

    rows = []
    for row in csvreader:
        rows.append(row)

	# get total number of rows
#    print("Total no. of rows: %d"%(csvreader.line_num))

# printing the field names
#print('Field names are:' + ', '.join(field for field in fields))

# printing first 5 rows
#print('\nFirst 5 rows are:\n')
#for row in rows[:5]:
#	# parsing each column of a row
#	for col in row:
#		print("%10s"%col,end=" "),
#	print('\n')


# Load data from CSV file
#Google collab 
#from google.colab import drive #worked on this in Google collab because I was having too many issues in WSL. I wasn't able to push my current progress so I started over, so I don't think there's any visible commits. I'm only able to push from my docker wsl? 
#drive.mount('/content/drive')
#data = pd.read_csv('/content/drive/MyDrive/Colab Notebooks/FinalProject/archive.csv', header = 2, low_memory = False, parse_dates=['time'])

data = pd.read_csv('archive.csv', header = 2, low_memory = False, parse_dates=['time'])
#print(data.dtypes)
#print(data.header())

# Check for missing values
if data.isnull().values.any():
    print("Warning: Data contains missing values.")
    
#Change data types
data['time'] = pd.to_datetime(data['time'], errors='coerce')
data['temperature_2m (°F)'] = pd.to_numeric(data['temperature_2m (°F)'], errors='coerce')
data['apparent_temperature (°F)'] = pd.to_numeric(data['apparent_temperature (°F)'], errors='coerce')
data['relativehumidity_2m (%)'] = pd.to_numeric(data['relativehumidity_2m (%)'], errors='coerce')
data['windspeed_10m (m/s)'] = pd.to_numeric(data['windspeed_10m (m/s)'], errors='coerce')
data['windspeed_100m (m/s)'] = pd.to_numeric(data['windspeed_100m (m/s)'], errors='coerce')
data['pressure_msl (hPa)'] = pd.to_numeric(data['pressure_msl (hPa)'], errors='coerce')

# Define input parameters
T_air = data['temperature_2m (°F)'] * 5 / 9 + 273.15  # air temperature (K)
T_sfc = data['apparent_temperature (°F)'] * 5 / 9 + 273.15  # surface temperature (K)
RH = data['relativehumidity_2m (%)'] / 100  # relative humidity
windspeed = data['windspeed_10m (m/s)']  # wind speed (m/s)
pressure = data['pressure_msl (hPa)'] * 100 # pressure (Pa)

# Define constants
sigma = 5.67e-8  # Stefan-Boltzmann constant
emissivity = 0.9  # surface emissivity
cp_air = 1004  # specific heat of air (J/kg/K)
cp_sfc = 1004  # specific heat of surface (J/kg/K)
k_air = 0.0257  # thermal conductivity of air (W/m/K)
rho_air = 1.225  # density of air (kg/m^3)
h_conv = 15  # convective heat transfer coefficient (W/m^2/K)
h_rad = 5  # radiative heat transfer coefficient (W/m^2/K)
z_0 = 0.01  # surface roughness length (m)

#data.plot(x='time', y = T_air)
#plt.xlabel('Time')
#plt.ylabel('T_air')
#plt.show()

# convert index to datetime format
data = data.set_index('time')
data.index = pd.to_datetime(data.index)
#print(data.index)
for i in range(len(data) - 1):
    time_diff_seconds = (data.index[i+1] - data.index[i]).total_seconds()

# Define surface heat balance equation
def surface_heat_balance(T_sfc, T_air, RH, windspeed):
    # Saturation vapor pressure (Pa)
    esat = 611.2 * np.exp(17.67 * (T_air - 273.15) / (T_air - 29.65))
    # Vapor pressure (Pa)
    e = RH * esat
    # Specific humidity (kg/kg)
    q = 0.622 * e / (pressure - 0.378 * e)
    # Latent heat of vaporization (J/kg)
    L = 2.5e6 - 2.3e3 * (T_air - 273.15)
    # Sensible heat flux (W/m^2)
    H_sens = rho_air * cp_air * windspeed * (T_sfc - T_air)
    # Latent heat flux (W/m^2)
    H_latent = rho_air * L * windspeed * q / time_diff_seconds
    # Net radiation flux (W/m^2)
    R_net = emissivity * sigma * T_sfc ** 4 - h_rad * (T_sfc - T_air)
    # Surface energy balance (W/m^2)
    SEB = R_net - H_sens - H_latent
    return SEB

# Interpolate windspeed data to surface level
f_windspeed = interpolate.interp1d(data['windspeed_100m (m/s)'], np.arange(len(data)), fill_value='extrapolate')

# Define the wind speed function
def windspeedFunc(windspeed):
    # Check if any windspeed value is NaN
    if windspeed.isna().any():
        return T_sfc

    # Convert windspeed from mph to m/s
    mph_to_mps = 0.44704
    interp_windspeed = f_windspeed(windspeed * mph_to_mps)

    # Check if the interpolated windspeed is within range
    if np.isnan(interp_windspeed) or interp_windspeed < 0 or interp_windspeed >= len(data['windspeed_100m (m/s)']):
        return T_sfc

    # Retrieve windspeed at surface level
    windspeed_sfc = int(data['windspeed_100m (m/s)'][np.round(interp_windspeed).astype(int)])
    return windspeed_sfc

windspeed_sfc = windspeedFunc(windspeed)

# Define the surface temperature function
def surface_temp(T_sfc, T_air):
    # Calculate surface temperature using surface energy balance
    T_sfc_new = T_sfc.copy()
    T_sfc_new += 0.1 * (T_air - T_sfc) + 0.5 * (windspeed_sfc - windspeed)

    return T_sfc_new

# Define the heat capacity of the surface layer (J/m^2/K)
rho_sfc = 1000 #kg/m^3 (typical for freshwater)
cp_sfc = 4180 #J/kg/K (typical for freshwater)
thickness_sfc = 1 #1 meter
C_sfc = 1000 * rho_sfc * cp_sfc * thickness_sfc

# Calculate surface temperature using surface energy balance
T_sfc_new = T_sfc.copy()

for i in range(100):
    T_sfc_new = 2*surface_temp(T_sfc_new, T_air)/3
    SEB = surface_heat_balance(T_sfc_new, T_air, RH, windspeed_sfc)
    dt = 3600 #seconds
    dT_sfc = SEB * dt / C_sfc
    T_sfc_new += dT_sfc

data.loc[:,['temperature_2m (°F)','apparent_temperature (°F)']].plot()
plt.xlabel('Year')
plt.ylabel('Temperature (°F)')
plt.title('Temperature')
plt.show()

data.loc[:,['windspeed_10m (m/s)','windspeed_100m (m/s)']].plot()
plt.xlabel('Year')
plt.ylabel('Wind Speed (m/s)')
plt.title('Wind Speed')
plt.show()

data.loc[:,['relativehumidity_2m (%)']].plot()
plt.xlabel('Year')
plt.ylabel('Relative Humidity (%)')
plt.title('Relative Humidity')
plt.show()

data.loc[:,['pressure_msl (hPa)']].plot()
plt.xlabel('Year')
plt.ylabel('Pressure (hPa)')
plt.title('Pressure')
plt.show()

T_air.plot()
plt.xlabel('Row')
plt.ylabel('Apparent Temperature (K)')
plt.title('Apparent Temperature (K)')
plt.show()

T_sfc.plot()
plt.xlabel('Row')
plt.ylabel('Surface Temperature (K)')
plt.title('Surface Temperature (K)')
plt.show()

RH.plot()
plt.xlabel('Row')
plt.ylabel('Relative Humidity (%)')
plt.title('Relative Humidity (%)')
plt.show()

windspeed.plot()
plt.xlabel('Row')
plt.ylabel('Wind Speed at 10 meters')
plt.title('Wind Speed at 10 meters')
plt.show()

pressure.plot()
plt.xlabel('Row')
plt.ylabel('Pressure (Pa)')
plt.title('Pressure (Pa)')
plt.show()

T_sfc_new.plot()
plt.xlabel('Row')
plt.ylabel('New Surface Temperature (K)')
plt.title('New Surface Temperature (K)')
plt.show()
#start_year = data.index(0)
#end_year = data.index(1000)
#fig, ax = plt.subplots()
#T_air.loc[start_year:end_year]. plot(style ='.', ax=ax)

# Set the x and y axis labels and plot title
#plt.xlabel('Time')
#plt.ylabel('Temperature (K)')
#plt.title('2-Meter Temperature')
#plt.show()

# Plot the temperatures against the time index
#fig, ax = plt.subplots()
#ax.plot(T_air.index, T_air)

# Set the x-axis to display the years
#years = mdates.YearLocator()
# Only show the year once
#ax.xaxis.set_major_locator(ticker.MaxNLocator(integer=True))
#ax.xaxis.set_major_locator(years)
#ax.xaxis.set_major_formatter(mdates.AutoDateFormatter(years))
#plt.setp(ax.xaxis.get_majorticklabels(), rotation=45)

# Add axis labels and a title
#ax.set_xlabel('Year')
#ax.set_ylabel('Temperature (°F)')
#ax.set_title('2m Temperature over Time')
#plt.show()

##I tried to redo (unsuccessfully) the EES code I already made 

# Preliminary Info
F1 = 'water'  # NO GLYCOL - Just water
P_atm = 101325 # atmpospheric pressure [Pa]
P_ref = 150*1000  # Pressure out of impeller pump [Pa]
flo = [i * (1.66667*10**-5) for i in [1,5,10,15,20,25,30,35,40,45,50]]  # From pump curve [L/min, m^3/s] 0,5,10,15,20,25,30,35,40,45,50,55,60,65
T_in = [275,280,285,290,295,300,305,310,315,320,325] # wanted to put T_sfc values here. Was struggling to make to work so I applied a range that encompasses values in data frame
T_1 = [275]*11  # tried to create the lists to go into the array. None values kept returning errors, but these aren't right
T_2 = [280]*11
T_3 = [285]*11
T_4 = [290]*11
T_5 = [295]*11
T = [T_in,T_1,T_2,T_3,T_4,T_5]  # Temperature at each node [K]
h = [None] * 6  # Enthalpy at each node [J/kg]
q_dot_total = None  # Total heat rejection [W]


# Battery information
R_cell = 0.0027  # Ohm
R_batt = R_cell * 26
i_motor_peak = 550  # Peak current [A]
i_motor_cont = 220  # Continuous current [A]
i_per_b = i_motor_cont / 3  # Current to each battery when wired in parallel [A]
P_heat_b = (R_batt * i_per_b ** 2) / 1000  # Peak heat rejection from 1 battery [kW]

# Motor Information
P_cont_mot = 15  # Continuous power [kW]
P_peak_mot = 43  # Peak power [kW]
P_heat_m = P_cont_mot * 0.1  # 10% of power to motor goes to heat (92% eff motor, 10% is worst case) [kW]

for i, T_in_val in enumerate(T_in):
  # Calculate mass flow rate of water in cooling loop
  rho_w = PropsSI('D', 'T', T[0][i], 'P', P_ref, 'Water')
  m_dot_w_array = []
  for x in flo:
      m_dot_w_array.append(x * rho_w)
  m_dot_w_array = np.array(m_dot_w_array)


  #print(T_val)
  # Node 1 - Battery 1
  h[0] = PropsSI('H', 'T', T[0][i], 'P', P_ref, 'Water')
  #h[0] = water.H(T=T[0], P=P_ref)
  q_dot_b1 = P_heat_b
  h[1] = ((m_dot_w_array  * h[0]) + q_dot_b1) / m_dot_w_array 
  #print(h[1])
  T[1][i] = PropsSI('T', 'H', h[1][i], 'P', P_ref, 'Water')
  #print(len(T))
  #print(T)
  #print("T[j][1]:",T[j][1])
  #T[1] = water.T_hp(P=P_ref, h=h[1])

  # Node 2 - Battery 2
  q_dot_b2 = P_heat_b
  h[2] = (m_dot_w_array  * h[1] + q_dot_b2) / m_dot_w_array 
  T[1][i] = PropsSI('T', 'H', h[2][i], 'P', P_ref, 'Water')
  #print("T[j][2]:",T[j][2])
  #T[2] = water.T_hp(P=P_ref, h=h[2])

  # Node 3 - Battery 3
  q_dot_b3 = P_heat_b
  h[3] = (m_dot_w_array  * h[2] + q_dot_b3) / m_dot_w_array 
  T[3][i] = PropsSI('T', 'H', h[3][i], 'P', P_ref, 'Water')
  #T[3] = water.T_hp(P=P_ref, h=h[3])

  # Node 4 - Controller
  q_dot_con = 1  # Controller heat rejection not yet known [kW]
  h[4] = (m_dot_w_array  * h[3] + q_dot_con) / m_dot_w_array 
  #T[4] = water.T_hp(P=P_ref, h=h[4])
  T[4][i] = PropsSI('T', 'H', h[4][i], 'P', P_ref, 'Water')
  q_dot_total = q_dot_con + q_dot_b1 + q_dot_b2 + q_dot_b3 + P_heat_m

  # Node 5 - Motor
  h[5] = (m_dot_w_array  * h[4] + P_heat_m) / m_dot_w_array 
  #T[5] = water.T_hp(P=P_ref, h=h[5])
  T[5][i] = PropsSI('T', 'H', h[5][i], 'P', P_ref, 'Water')

q_dot_hx_array = []
for i, T_in_val in enumerate(T_in):
  # Heat exchanger
  epsilon = 0.6 # value for effectiveness of heat exchanger
  # Calculation of heat exchanger effectiveness
  cp_H = PropsSI('C', 'T', T[4][i], 'P', P_ref, 'Water')  # specific heat of hot water
  cp_C = PropsSI('C', 'T', T[0][i], 'P', P_ref, 'Water')   # specific heat of lake water
  C_dot_H = m_dot_w_array[i]*cp_H 
  C_dot_C = m_dot_w_array[i]*cp_C
  C_dot_min = np.min([C_dot_C, C_dot_H])
  T_H_in = T[4][i]
  #print(T_H_in)
  T_C_in = T[0][i]
  q_dot_hx_max = C_dot_min*(T_H_in-T_C_in)
  q_dot_hx = epsilon*q_dot_hx_max
  q_dot_hx_array.append(q_dot_hx)
  #epsilon = q_dot_hx/q_dot_hx_max  # solves for true q_dot across heat exchanger


delta_T_list = []
num_cols = len(T[0])

for col_index in range(num_cols):
  for row_index, row in enumerate(T):
    if row_index == 0:
      continue # skip the first row
    delta_T = row[col_index] - T[0][col_index]
    #print(delta_T)
    delta_T_list.append(delta_T)    # trying to figure out the change in temperature to determine the effective of mass flow rate. Should go up with mass flow rate but it doesn't 

#print(len(delta_T_list))

# Plot each line
#plt.scatter(m_dot_w_array, T[1], label='T 1')
#plt.scatter(m_dot_w_array, T[2], label='T 2')
#plt.scatter(m_dot_w_array, T[3], label='T 3')
#plt.scatter(m_dot_w_array, T[4], label='T 4')
#plt.scatter(m_dot_w_array, T[5], label='T 5')
# Add axis labels and legend
plt.xlabel('Mass Flow Rate')
plt.ylabel('Temperature')
plt.legend()
# Show the plot
#plt.show()

# Plot each line
plt.plot(m_dot_w_array,q_dot_hx_array)
# Add axis labels and legend
plt.xlabel('Mass Flow Rate')
plt.ylabel('Heat Released')
plt.legend()
# Show the plot
plt.show()